package command;

import model.Board;

/**
 * Command for each direction
 * Created by tbernard on 02/11/15.
 */
public class CommandImpl {

    private Board board;
    public CommandImpl(Board board){
        this.board=board;

    }
    public void moveLeft() {
        board.moveAndGenerate(Board.Direction.LEFT);
        board.commit();

    }

    public void moveRight() {
        board.moveAndGenerate(Board.Direction.RIGHT);
        board.commit();

    }

    public void moveUp() {

        board.moveAndGenerate(Board.Direction.TOP);
        board.commit();
    }

    public void moveDown() {
        board.moveAndGenerate(Board.Direction.BOTTOM);
        board.commit();
    }




}
