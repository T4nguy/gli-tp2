package sample;

import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.Optional;
import java.util.ResourceBundle;


import command.CommandImpl;
import command.Macro;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import model.Board;
import model.BoardImpl;
import services.TileModule;
import services.TileService;

import javax.inject.Inject;

/**
 * Controller
 */
public class Controller implements Initializable, Observer {

    @FXML
    GridPane myMatrix;

    @FXML
    MenuItem new_game;

    @FXML
    MenuItem close;

    @FXML
    MenuItem m_help_about;

    @Inject
    private TileService tileService;//Inject my tile service



    private Label[][] label = new Label[4][4];//label to display value
    private BoardImpl board;//my board
    private Scene scene;
    private Stage stage;
    final double MAX_FONT_SIZE = 30.0; // define max font size


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }


    /**
     * Initialize listener
     */
    public void init(){
        this.board.initBoard();

        Macro macro = new Macro();
        CommandImpl cmd = new CommandImpl(board);

        //listen action new game
        new_game.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                cleanLabels();
                label = new Label[4][4];
                init();



            }
        });
        //listen action "about"
        m_help_about.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                about();

            }
        });

        //listen action close
        close.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {


                Main.closeStage();
            }
        });

        createLabelsGrid();

        //listen action key Left, Right, Up, Down
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

            public void handle(KeyEvent ke) {

                if (ke.getCode() == KeyCode.LEFT) {
                    macro.run(cmd::moveLeft);

                }
                else if (ke.getCode() == KeyCode.RIGHT) {
                    macro.run(cmd::moveRight);

                }
                else if (ke.getCode() == KeyCode.UP) {
                    macro.run(cmd::moveUp);

                }
                else if (ke.getCode() == KeyCode.DOWN) {
                    macro.run(cmd::moveDown);

                }



            }
        });
    }


    /**
     * Make label empty
     */
    public void cleanLabels(){
        for (int i = 0; i < label.length; i++) {
            for (int j = 0; j < label[i].length; j++) {

                    label[i][j].setText("");


            }
        }
    }


    /**
     * Create label according to rank
     */
    public void createLabelsGrid(){

        int pow = 2;
        for (int i = 0; i < label.length; i++) {
            for (int j = 0; j < label[i].length; j++) {
                label[i][j] = new Label();
                label[i][j].setFont(new Font(MAX_FONT_SIZE)); // set to Label);
                label[i][j].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

                //set border on Tile
                label[i][j].setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, null)));

                //if the are a rank
                if(board.getTile(j+1,i+1)!=null){
                     pow = tileService.pow((board.getTile(j + 1, i + 1).getRank()));
                    label[i][j].setText(String.valueOf(pow));
                    tileService.colorLabel(label[i][j],pow);
                    //label[i][j].setStyle("-fx-background-color: rgba(255, 255, 255, 1);-fx-alignment: center;");

                }
                else{
                    label[i][j].setText("");//display empty
                    label[i][j].setStyle("-fx-background-color: cornsilk ;-fx-alignment: center;");
                }

                myMatrix.setHalignment(label[i][j], HPos.CENTER);
                myMatrix.add(label[i][j], i, j);
            }
        }
    }

    /**
     * Set board
     * @param board
     */
    public void setBoard(BoardImpl board) {
        this.board = board;
    }

    /**
     * Get board
     * @return board
     */
    public Board getBoard(){
        return this.board;
    }

    /**
     * Set scene
     * @param scene
     */
    public void setScene(Scene scene){
        this.scene = scene;
    }

    public void setStage(Stage scene){
        this.stage = stage;
    }
    public Scene getScene(){
        return this.scene;
    }

    /**
     * Update grid if action
     * @param o
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {

        if(o instanceof BoardImpl){
            if(arg=="lose"){
                youLose();// call alert You lose !
            }
                //update grid
                cleanLabels();
                createLabelsGrid();


        }

    }


    /**
     * Method about, display Alert information
     */
    public void about(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("My512 by Tanguy BERNARD!");

        alert.showAndWait();
    }

    /**
     * Method which create an alert if you lose
     */
    public void youLose(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("You LOSE !!!");
        alert.setHeaderText(null);
        alert.setContentText("Try again ?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            // ... user chose OK
            cleanLabels();
            label = new Label[4][4];
            init();
        } else {
            // ... user chose CANCEL or closed the dialog
            Main.closeStage();
        }


    }

}
