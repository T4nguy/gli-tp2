package services;

import javafx.scene.control.Label;

/**
 * Service which provide some method for the tile
 * Created by tbernard on 07/11/15.
 */
public interface TileService {

    /**
     * return 2 ** rank
     * @param rank rank
     * @return 2 ** rank
     */
    int pow(int rank);

    /**
     * Change color label depending on pow
     * @param label
     * @param pow
     */
    void colorLabel(Label label, int pow);

}
