package command;

import model.Board;

/**
 * Created by tbernard on 22/10/15.
 */
public interface Command {

    void execute();

}