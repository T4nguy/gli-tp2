package services;

import javafx.scene.control.Label;

/**
 * Created by tbernard on 07/11/15.
 */
public class ConcreteTileService  implements  TileService{

    /**
     * Return pow for displaying
     * @param rank rank
     * @return
     */
    @Override
    public int pow(int rank) {
        return (int)Math.pow(2,rank);
    }

    /**
     * Use color on Label
     * @param label
     * @param pow
     */
    @Override
    public void colorLabel(Label label, int pow){

        switch (pow){
            case 2:
                label.setStyle("-fx-background-color: rgb(238, 248, 218) ;-fx-alignment: center;");
                break;
            case 4:
                label.setStyle("-fx-background-color: rgb(237, 224, 200) ;-fx-alignment: center;");
                break;
            case 8:
                label.setStyle("-fx-background-color: rgb(242, 177, 121) ;-fx-alignment: center;");
                break;
            case 16:
                label.setStyle("-fx-background-color: rgb(245, 199, 99) ;-fx-alignment: center;");
                break;
            case 32:
                label.setStyle("-fx-background-color: rgb(246, 124, 95) ;-fx-alignment: center;");
                break;
            case 64:
                label.setStyle("-fx-background-color: rgb(246, 94, 59) ;-fx-alignment: center;");
                break;
            case 128:
                label.setStyle("-fx-background-color: rgb(237, 207, 114) ;-fx-alignment: center;");
                break;
            case 256:
                label.setStyle("-fx-background-color: rgb(237, 204, 97) ;-fx-alignment: center;");
                break;
            case 512:
                label.setStyle("-fx-background-color: rgb(237, 200, 80) ;-fx-alignment: center;");
                break;
            case 1024:
                label.setStyle("-fx-background-color: rgb(237, 197, 63) ;-fx-alignment: center;");
                break;
            case 2048:
                label.setStyle("-fx-background-color: rgb(237, 194, 46) ;-fx-alignment: center;");
                break;
            default:
                label.setStyle("-fx-background-color: purple ;-fx-alignment: center;");
                break;
        }

    }
}
