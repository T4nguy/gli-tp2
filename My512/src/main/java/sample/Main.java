package sample;

import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.BoardImpl;
import services.TileModule;
import services.TileService;


public class Main extends Application {

    static Stage stage;

    @Override
    public void start(Stage primaryStage) throws Exception{
        stage = primaryStage;

        //Dependency injection
        Injector injector = Guice.createInjector(new TileModule());

        //load fxm to create scene
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("/my512.fxml")
        );


        Parent root = loader.load();//First load;
        Controller myController = (Controller) loader.getController();
        BoardImpl board1 = new BoardImpl(4);// 4 * 4

        //Create scene and set dimensions
        Scene scene = new Scene(root, 400, 400);

        primaryStage.setTitle("My512");//set title
        primaryStage.setScene(scene);

        myController.setScene(scene);//set scene
        myController.setBoard(board1);//set board
        board1.addObserver(myController);//board is observable

        injector.getInstance(TileService.class);//inject TileService
        injector.injectMembers(myController);//to my Controller

        myController.init();//init key listener

        primaryStage.show();//show view


    }

    /**
     * close the view
     */
    public static void closeStage(){
        stage.close();
    }

    /**
     * Launch my application view
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}