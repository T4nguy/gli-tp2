package model;

/**
 * Created by plouzeau on 2014-10-09.
 */
public interface Tile {

    /**
     * Return rank of Tile
     * @return rank
     */
    int getRank();

    /**
     * Increment rank of Tile
     */
    void incrementRank();
}
