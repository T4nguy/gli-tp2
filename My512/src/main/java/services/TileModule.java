package services;

import com.google.inject.AbstractModule;

/**
 * Configure Service
 * Created by tbernard on 06/11/15.
 */
public class TileModule extends AbstractModule {


    @Override
    protected void configure() {
        bind(TileService.class).to(ConcreteTileService.class);
    }
}
