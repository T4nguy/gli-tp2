# **Sujet de travaux  pratiques M2 GL, UE GLI** #

## **Objectifs du TP**

Il s’agit de pratiquer un peu la création d’une interface JavaFX, en utilisant quelques classes de la bibliothèque et en mettant en œuvre le patron de conception MVC (ou ses amis MVP et MVVM).

## **Réalisation**
L'application a été mis en oeuvre selon le pattern MVC. Le controleur qui est observateur de la vue, va mettre à jour le modèle puis actualiser la vue.

Les actions permises au clavier sont implémentées en utilisant le pattern Command.

J'ai souhaité utiliser un système de service pour les Tiles avec un injecteur de dépendance : [Google Guice](https://github.com/google/guice)

Ce service fournit deux méthodes. Une pour mettre à la puissance la valeur du rang et la seconde fournit une couleur pour chaque puissance.

L'interface a été construite en FXML via Scene Builder.

## **Screenshot**

![game1.png](https://bitbucket.org/repo/AAd9B7/images/1877396246-game1.png)

**BERNARD Tanguy**